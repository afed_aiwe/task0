package mediator;

import handler.CollectionOrderHandler;
import handler.DeleteOrderHandler;
import handler.HashOrderHandler;
import handler.OrderHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import order.Order;

public class Manager {
    private Queue<Order> orders;
    private Map<Integer, OrderHandler> handlerHashMap;
    private OrderHandler hashOrderHandler;
    private OrderHandler collectionOrderHandler;
    private OrderHandler deleteOrderHandler;

    public Manager(Queue<Order> orders) {
        this.orders = orders;
        handlerHashMap = new HashMap<Integer, OrderHandler>();
        hashOrderHandler = new HashOrderHandler();
        collectionOrderHandler = new CollectionOrderHandler();
        deleteOrderHandler = new DeleteOrderHandler();
        handlerHashMap.put(0, hashOrderHandler);
        handlerHashMap.put(1, collectionOrderHandler);
        handlerHashMap.put(2, deleteOrderHandler);
    }

    public void process() {
        int count = orders.size();
        for(int i = 0; i < count; i++) {
            int mapKey = i%3;
            OrderHandler orderHandler = handlerHashMap.get(mapKey);
            orderHandler.carryOut();
        }
    }
}
