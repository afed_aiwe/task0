package handler;

import java.security.NoSuchAlgorithmException;
import order.Order;

public abstract class OrderHandler {
    protected Order order;

    public OrderHandler() {

    }

    public abstract void carryOut();

    public void setOrder(Order order) {
        this.order = order;
    };

}
