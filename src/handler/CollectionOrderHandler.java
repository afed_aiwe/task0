package handler;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import order.Order;

public class CollectionOrderHandler extends OrderHandler {
    HashSet<String> hashSet = new HashSet<>();

    public CollectionOrderHandler() {
        super();
        int i = 0;
        while (i < 10) {
            hashSet.add("order" + i);
            i++;
        }
    }

    @Override
    public void setOrder(Order order) {
        super.setOrder(order);
    }

    @Override
    public void carryOut() {
        if (hashSet.contains(order.getName())) {
            System.out.println("special");
        } else {
          System.out.println("usual");
        }
    }

}
