package handler;

import order.Order;

public class DeleteOrderHandler extends OrderHandler {

    public DeleteOrderHandler() {
        super();
    }

    @Override
    public void setOrder(Order order) {
        super.setOrder(order);
    }

    @Override
    public void carryOut() {
        String name = order.getName();
        name = name.replaceAll("[oae]", "");
        System.out.println(name);
    }
}
