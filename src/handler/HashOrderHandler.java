package handler;

import com.sun.javafx.binding.StringFormatter;
import com.sun.tools.doclets.standard.Standard;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import order.Order;

public class HashOrderHandler extends OrderHandler {
    private MessageDigest md;

    public  HashOrderHandler() {
        super();

    }


    @Override
    public void carryOut() {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String str = LocalDate.now() + " " + order.getTimeNow().toString();

        byte[] hashInBytes = md.digest(str.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for(byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }

        System.out.println(sb.toString());
    }

    @Override
    public void setOrder(Order order) {
        super.setOrder(order);
    }
}
