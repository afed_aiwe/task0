package order;

import java.time.LocalTime;

public class Order {

    private String name;
    private LocalTime timeNow;

    public Order() {
    }

    public Order(String name, LocalTime timeNow) {
        this.name = name;
        this.timeNow = timeNow;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getTimeNow() {
        return timeNow;
    }

    public void setTimeNow(LocalTime timeNow) {
        this.timeNow = timeNow;
    }
}
