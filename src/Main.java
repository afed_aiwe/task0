import com.sun.tools.corba.se.idl.constExpr.Or;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import mediator.Manager;
import order.Order;

public class Main {

    public static void main(String[] args) {

        Queue<Order> orderQueue = new LinkedList<Order>();
        int n = 0;
        String nameOrder;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Input n: ");
        n = scanner.nextInt();

        for (int i = 0; i < n; i++){
            System.out.println("Input order name");
            nameOrder = scanner.next();
            orderQueue.add(queuing(nameOrder, LocalTime.now()));
        }

        Manager manager = new Manager(orderQueue);
        manager.process();
    }

    public static Order queuing (String name, LocalTime timeNow) {
        return new Order(name, timeNow);
    }

}
